//
//  CollectionViewCell.h
//  CollectionView sample
//
//  Created by Banu Desi Antoro on 4/21/14.
//  Copyright (c) 2014 Inarita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@end
