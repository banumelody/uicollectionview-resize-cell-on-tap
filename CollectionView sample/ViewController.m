//
//  ViewController.m
//  CollectionView sample
//
//  Created by Banu Desi Antoro on 4/21/14.
//  Copyright (c) 2014 Inarita. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewCell.h"

@interface ViewController () {
    
}

@end

@implementation ViewController {
    UIView *subView;
    UITapGestureRecognizer *singleTap;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 100;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionViewCell" forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"INDEX : %d",indexPath.row);
    
    CollectionViewCell *cell = (CollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    CGRect rect = cell.frame;
    rect.size = CGSizeMake(2*cell.frame.size.width + 20, 2*cell.frame.size.height + 10);
    
    subView = [[UIView alloc] initWithFrame:rect];
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];

    subView.backgroundColor = [UIColor blueColor];
    [collectionView addSubview:subView];
    [self.view addGestureRecognizer:singleTap];
}

- (void) handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [subView removeFromSuperview];
    [self.view removeGestureRecognizer:singleTap];
}

@end
